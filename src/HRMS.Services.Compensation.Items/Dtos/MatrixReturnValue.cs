﻿namespace HRMS.Services.Compensation.Items.Dtos
{
    public enum MatrixReturnValue
    {
        Default = 0,

        Min = 1,

        Low = 2,

        High = 3,

        Max = 4
    }
}
