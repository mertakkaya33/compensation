﻿namespace HRMS.Services.Compensation.Items.Entities
{
    public partial class Matrix
    {
        public string FormulaNameIsRequired { get; set; }

        public bool RatingFromIsRequired { get; set; }

        public bool RatingFromInclusiveIsRequired { get; set; }

        public float RatingToIsRequired { get; set; }

        public bool RatingToInclusiveIsRequired { get; set; }

        public float RatioFromIsRequired { get; set; }

        public bool RatioFromInclusiveIsRequired { get; set; }

        public float RatioToIsRequired { get; set; }

        public bool RatioToInclusiveIsRequired { get; set; }

        public string CustomCriteria0IsRequired { get; set; }

        public string CustomCriteria0ValueIsRequired { get; set; }

        public string CustomCriteria0FromValueIsRequired { get; set; }

        public bool CustomCriteria0FromInclusiveIsRequired { get; set; }

        public string CustomCriteria0ToValueIsRequired { get; set; }

        public bool CustomCriteria0ToInclusiveIsRequired { get; set; }

        public float MinIsRequired { get; set; }

        public float LowIsRequired { get; set; }

        public float DefaultIsRequired { get; set; }

        public float HighIsRequired { get; set; }

        public float MaxIsRequired { get; set; }
    }
}
