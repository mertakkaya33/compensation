﻿namespace HRMS.Services.Compensation.Items.Entities
{
    public partial class Matrix
    {
        public string FormulaNameTitle { get; set; }

        public bool RatingFromTitle { get; set; }

        public bool RatingFromInclusiveTitle { get; set; }

        public float RatingToTitle { get; set; }

        public bool RatingToInclusiveTitle { get; set; }

        public float RatioFromTitle { get; set; }

        public bool RatioFromInclusiveTitle { get; set; }

        public float RatioToTitle { get; set; }

        public bool RatioToInclusiveTitle { get; set; }

        public string CustomCriteria0Title { get; set; }

        public string CustomCriteria0ValueTitle { get; set; }

        public string CustomCriteria0FromValueTitle { get; set; }

        public bool CustomCriteria0FromInclusiveTitle { get; set; }

        public string CustomCriteria0ToValueTitle { get; set; }

        public bool CustomCriteria0ToInclusiveTitle { get; set; }

        public float MinTitle { get; set; }

        public float LowTitle { get; set; }

        public float DefaultTitle { get; set; }

        public float HighTitle { get; set; }

        public float MaxTitle { get; set; }
    }
}
