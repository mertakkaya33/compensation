﻿using HRMS.Common.Types;

namespace HRMS.Services.Compensation.Items.Entities
{
    public partial class Matrix : BaseAuditableEntity
    {
        public string Name { get; set; }

        public bool IsActive { get; set; }

        public string FormulaName { get; set; }

        public bool RatingFrom { get; set; }

        public bool RatingFromInclusive { get; set; }

        public float RatingTo { get; set; }

        public bool RatingToInclusive { get; set; }

        public float RatioFrom { get; set; }

        public bool RatioFromInclusive { get; set; }

        public float RatioTo { get; set; }

        public bool RatioToInclusive { get; set; }

        public string CustomCriteria0 { get; set; }

        public string CustomCriteria0Value { get; set; }

        public string CustomCriteria0FromValue { get; set; }

        public bool CustomCriteria0FromInclusive { get; set; }

        public string CustomCriteria0ToValue { get; set; }

        public bool CustomCriteria0ToInclusive { get; set; }

        public float Min { get; set; }

        public float Low { get; set; }

        public float Default { get; set; }

        public float High { get; set; }

        public float Max { get; set; }
    }
}
