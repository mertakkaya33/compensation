﻿using HRMS.Services.Compensation.Items.Dtos;
using System;

namespace HRMS.Services.Compensation.Items.Entities
{
    public class MatrixSettings
    {
        public Guid Id { get; set; }

        public Guid MatrixId { get; set; }

        public int DepartmentId { get; set; }

        public MatrixReturnValue ReturnValue { get; set; }
    }
}
