﻿using HRMS.Common.Messages;

namespace HRMS.Services.Compensation.Items.Commands
{
    public class CreateMatrixCommand : ICommand
    {
        public string Name { get; set; }

        public string FormulaName { get; set; }

        public int Count { get; set; } 
    }
}
