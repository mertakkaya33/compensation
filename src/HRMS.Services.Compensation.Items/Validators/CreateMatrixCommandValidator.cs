﻿using FluentValidation;
using HRMS.Services.Compensation.Items.Commands;

namespace HRMS.Services.Compensation.Items.Validators
{
    public class CreateMatrixCommandValidator : AbstractValidator<CreateMatrixCommand>
    {
        public CreateMatrixCommandValidator()
        {
            RuleFor(x => x.Name).NotNull()
                                .MinimumLength(1)
                                .MaximumLength(250);

            RuleFor(x => x.FormulaName).NotNull()
                                       .MinimumLength(1)
                                       .MaximumLength(250);
        }
    }
}
