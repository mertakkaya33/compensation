﻿using AutoMapper;
using HRMS.Services.Compensation.Items.Commands;
using HRMS.Services.Compensation.Items.Entities;
using System;

namespace HRMS.Services.Compensation.Items.Mappers
{
    public class MatrixProfile : Profile
    {
        public MatrixProfile()
        {
            CreateMap<CreateMatrixCommand, Matrix>().AfterMap((src, dest) => { dest.Id = Guid.NewGuid(); dest.IsActive = true; });
        }
    }
}
