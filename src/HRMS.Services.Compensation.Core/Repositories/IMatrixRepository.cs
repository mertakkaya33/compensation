﻿using HRMS.Services.Compensation.Items.Commands;
using System.Threading.Tasks;

namespace HRMS.Services.Compensation.Core.Repositories
{
    public interface IMatrixRepository
    {
        Task CreateAsync(CreateMatrixCommand matrix);
    }
}
