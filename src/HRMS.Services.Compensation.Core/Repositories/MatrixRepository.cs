﻿using AutoMapper;
using HRMS.Services.Compensation.Data;
using HRMS.Services.Compensation.Items.Commands;
using HRMS.Services.Compensation.Items.Entities;
using System.Threading.Tasks;

namespace HRMS.Services.Compensation.Core.Repositories
{
    public class MatrixRepository : IMatrixRepository
    {
        private readonly IUnitofWork uow;
        private readonly IMapper mapper;

        public MatrixRepository(IMapper mapper, IUnitofWork unitofWork)
        {
            uow = unitofWork;
            this.mapper = mapper;
        }

        public async Task CreateAsync(CreateMatrixCommand command)
        {
            Matrix matrix = mapper.Map<Matrix>(command);

            await uow.AsQueryable<Matrix>().AddAsync(matrix);

            await uow.SaveChangesAsync();
        }
    }
}
