﻿using AutoMapper;
using HRMS.Common.Exceptions;
using HRMS.Common.Handlers;
using HRMS.Common.RabbitMq;
using HRMS.Services.Compensation.Data;
using HRMS.Services.Compensation.Items.Commands;
using HRMS.Services.Compensation.Items.Entities;
using System.Threading.Tasks;

namespace HRMS.Services.Compensation.Core.Handlers.Commands
{
    public class CreateMatrixCommandHandler : ICommandHandler<CreateMatrixCommand>
    {
        private readonly IUnitofWork uow;
        private readonly IMapper mapper;

        public CreateMatrixCommandHandler(IUnitofWork unitofWork, IMapper mapper)
        {
            uow = unitofWork ?? throw new DependencyInjectionArgumentNullException(nameof(unitofWork));

            this.mapper = mapper ?? throw new DependencyInjectionArgumentNullException(nameof(mapper));

            
        }

        public async Task HandleAsync(CreateMatrixCommand command, ICorrelationContext correlationContext)
        {
            Matrix matrix = mapper.Map<Matrix>(command);

            await uow.AsQueryable<Matrix>().AddAsync(matrix);

            await uow.SaveChangesAsync();
        }
    }
}
