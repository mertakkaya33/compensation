﻿using AutoMapper;
using Consul;
using HRMS.Common.Consul;
using HRMS.Common.Dispatchers;
using HRMS.Common.Handlers;
using HRMS.Common.I18N;
using HRMS.Common.Mvc;
using HRMS.Common.RabbitMq;
using HRMS.Common.Swagger;
using HRMS.Common.Validators;
using HRMS.Services.Compensation.Core;
using HRMS.Services.Compensation.Core.Repositories;
using HRMS.Services.Compensation.Data;
using HRMS.Services.Compensation.Items;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System.Reflection;

namespace HRMS.Services.Compensation
{
    public class Startup
    {
        private readonly ILogger<Startup> logger;

        private static readonly string[] exposedHeaders = new[] { "" };

        public Startup(ILogger<Startup> logger, IConfiguration configuration)
        {
            this.logger = logger;

            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddI18N();

            services.AddCustomMvc(x =>
            {
                x.Filters.Add<ValidateModelAttribute>();
                x.Filters.Add<PagingActionFilterAttribute>();
            })
                .AddValidators(typeof(ItemsIdentifierType));

            services.AddConsul();

            services.AddSwaggerDocs();

            services.AddLogging(x => x.AddConsole());

            ConfigureIoc(services);

            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy", cors =>
                        cors.AllowAnyOrigin()
                            .AllowAnyMethod()
                            .AllowAnyHeader()
                            .AllowCredentials()
                            .WithExposedHeaders(exposedHeaders));
            });

            services.AddDispatchers();

            services.AddHandlers(typeof(CoreIdentifierType));

            services.AddRabbitMq();

            services.AddAutoMapper(Assembly.GetAssembly(typeof(ItemsIdentifierType)));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app,
                              IHostingEnvironment env,
                              IApplicationLifetime applicationLifetime,
                              IConsulClient client)
        {
            if (env.IsDevelopment() || env.EnvironmentName == "Docker")
            {
                app.UseDeveloperExceptionPage();

                if (env.EnvironmentName == "Docker")
                {
                    InitializeDatabase(app);
                }
            }

            var consulServiceId = app.UseConsul();

            applicationLifetime.ApplicationStopped.Register(() =>
            {
                client.Agent.ServiceDeregister(consulServiceId);
            });

            app.UseCors("CorsPolicy");

            app.UseErrorHandler();

            app.UseI18N();

            app.UseSwaggerDocs();

            app.UseRabbitMq();

            app.UseMvc();

        }

        private void ConfigureIoc(IServiceCollection services)
        {
            services.AddDbContext<ApplicationContext>(options => options.UseNpgsql(Configuration.GetConnectionString("DefaultConnection"), x => x.MigrationsAssembly("HRMS.Services.Compensation.Data")));

            services.AddScoped(typeof(DbContext), typeof(ApplicationContext));

            services.AddScoped(typeof(IUnitofWork), typeof(UnitofWork));

            services.AddScoped(typeof(IMatrixRepository), typeof(MatrixRepository));
        }

        private void InitializeDatabase(IApplicationBuilder app)
        {
            logger.LogInformation("Database migrating...");

            using (var serviceScope = app.ApplicationServices.CreateScope())
            {
                var provider = serviceScope.ServiceProvider;

                var db = provider.GetService<ApplicationContext>();

                db.Database.Migrate();
            }

            logger.LogInformation("Database migrated successfully.");
        }

    }
}
