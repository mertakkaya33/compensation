﻿using HRMS.Common.Dispatchers;
using HRMS.Common.Exceptions;
using HRMS.Services.Compensation.Core.Repositories;
using HRMS.Services.Compensation.Items.Commands;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Diagnostics;
using System.Threading.Tasks;

namespace HRMS.Services.Compensation.Controllers
{
    [Route("api/compensation/v1/matrix")]
    public class MatrixController : ServiceBaseController
    {
        private readonly IDispatcher dispatcher;
        private readonly IMatrixRepository matrixRepository;
        private ILogger<MatrixController> logger;

        public MatrixController(IDispatcher dispatcher, IMatrixRepository matrixRepository, ILogger<MatrixController> logger)
        {
            this.dispatcher = dispatcher ?? throw new DependencyInjectionArgumentNullException(nameof(dispatcher));

            this.matrixRepository = matrixRepository ?? throw new DependencyInjectionArgumentNullException(nameof(matrixRepository));

            this.logger = logger ?? throw new DependencyInjectionArgumentNullException(nameof(logger));
        }

        [HttpGet]
        public IActionResult Get()
        {
            return Ok();
        }

        [HttpGet]
        [Route("{id:guid}")]
        public IActionResult Get([FromRoute] Guid id)
        {
            return Ok();
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] CreateMatrixCommand matrix)
        {
            logger.LogInformation("Creating with cqrs");

            Stopwatch stopwatch = Stopwatch.StartNew();

            await dispatcher.SendAsync(matrix);

            stopwatch.Stop();

            logger.LogInformation($"Created in {stopwatch.ElapsedMilliseconds} milliseconds");

            stopwatch.Reset();


            logger.LogInformation("Creating with repository");

            stopwatch.Start();

            await matrixRepository.CreateAsync(matrix);

            stopwatch.Stop();

            logger.LogInformation($"Created in {stopwatch.ElapsedMilliseconds} milliseconds");

            return Accepted();
        }

        [HttpPut]
        public IActionResult Update()
        {
            return Ok();
        }


    }
}
