﻿using Microsoft.AspNetCore.Mvc;

namespace HRMS.Services.Compensation.Controllers
{

    [Route("api/compensation/v1/service")]
    public class ServiceController : ServiceBaseController
    {
        public ServiceController()
        {

        }

        [HttpGet]
        public IActionResult Ping()
        {
            return Ok();
        }
    }
}
