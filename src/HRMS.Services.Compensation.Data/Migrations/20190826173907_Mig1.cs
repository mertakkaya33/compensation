﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace HRMS.Services.Compensation.Data.Migrations
{
    public partial class Mig1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ModifiedByWithProxy",
                schema: "compensation",
                table: "matrices",
                newName: "modified_by_with_proxy");

            migrationBuilder.RenameColumn(
                name: "ModifiedBy",
                schema: "compensation",
                table: "matrices",
                newName: "modified_by");

            migrationBuilder.RenameColumn(
                name: "CreatedByWithProxy",
                schema: "compensation",
                table: "matrices",
                newName: "created_by_with_proxy");

            migrationBuilder.RenameColumn(
                name: "CreatedBy",
                schema: "compensation",
                table: "matrices",
                newName: "created_by");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "modified_by_with_proxy",
                schema: "compensation",
                table: "matrices",
                newName: "ModifiedByWithProxy");

            migrationBuilder.RenameColumn(
                name: "modified_by",
                schema: "compensation",
                table: "matrices",
                newName: "ModifiedBy");

            migrationBuilder.RenameColumn(
                name: "created_by_with_proxy",
                schema: "compensation",
                table: "matrices",
                newName: "CreatedByWithProxy");

            migrationBuilder.RenameColumn(
                name: "created_by",
                schema: "compensation",
                table: "matrices",
                newName: "CreatedBy");
        }
    }
}
