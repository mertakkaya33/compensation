﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace HRMS.Services.Compensation.Data.Migrations
{
    public partial class Mig : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "compensation");

            migrationBuilder.CreateTable(
                name: "matrices",
                schema: "compensation",
                columns: table => new
                {
                    id = table.Column<Guid>(type: "uuid", nullable: false),
                    create_at = table.Column<DateTime>(type: "timestamp", nullable: false),
                    modified_at = table.Column<DateTime>(type: "timestamp", nullable: false),
                    CreatedBy = table.Column<Guid>(nullable: false),
                    CreatedByWithProxy = table.Column<Guid>(nullable: false),
                    ModifiedBy = table.Column<Guid>(nullable: false),
                    ModifiedByWithProxy = table.Column<Guid>(nullable: false),
                    name = table.Column<string>(type: "varchar", maxLength: 250, nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    formula_name = table.Column<string>(type: "varchar", maxLength: 250, nullable: false),
                    RatingFrom = table.Column<bool>(nullable: false),
                    RatingFromInclusive = table.Column<bool>(nullable: false),
                    RatingTo = table.Column<float>(nullable: false),
                    RatingToInclusive = table.Column<bool>(nullable: false),
                    RatioFrom = table.Column<float>(nullable: false),
                    RatioFromInclusive = table.Column<bool>(nullable: false),
                    RatioTo = table.Column<float>(nullable: false),
                    RatioToInclusive = table.Column<bool>(nullable: false),
                    CustomCriteria0 = table.Column<string>(nullable: true),
                    CustomCriteria0Value = table.Column<string>(nullable: true),
                    CustomCriteria0FromValue = table.Column<string>(nullable: true),
                    CustomCriteria0FromInclusive = table.Column<bool>(nullable: false),
                    CustomCriteria0ToValue = table.Column<string>(nullable: true),
                    CustomCriteria0ToInclusive = table.Column<bool>(nullable: false),
                    Min = table.Column<float>(nullable: false),
                    Low = table.Column<float>(nullable: false),
                    Default = table.Column<float>(nullable: false),
                    High = table.Column<float>(nullable: false),
                    Max = table.Column<float>(nullable: false),
                    FormulaNameIsRequired = table.Column<string>(nullable: true),
                    RatingFromIsRequired = table.Column<bool>(nullable: false),
                    RatingFromInclusiveIsRequired = table.Column<bool>(nullable: false),
                    RatingToIsRequired = table.Column<float>(nullable: false),
                    RatingToInclusiveIsRequired = table.Column<bool>(nullable: false),
                    RatioFromIsRequired = table.Column<float>(nullable: false),
                    RatioFromInclusiveIsRequired = table.Column<bool>(nullable: false),
                    RatioToIsRequired = table.Column<float>(nullable: false),
                    RatioToInclusiveIsRequired = table.Column<bool>(nullable: false),
                    CustomCriteria0IsRequired = table.Column<string>(nullable: true),
                    CustomCriteria0ValueIsRequired = table.Column<string>(nullable: true),
                    CustomCriteria0FromValueIsRequired = table.Column<string>(nullable: true),
                    CustomCriteria0FromInclusiveIsRequired = table.Column<bool>(nullable: false),
                    CustomCriteria0ToValueIsRequired = table.Column<string>(nullable: true),
                    CustomCriteria0ToInclusiveIsRequired = table.Column<bool>(nullable: false),
                    MinIsRequired = table.Column<float>(nullable: false),
                    LowIsRequired = table.Column<float>(nullable: false),
                    DefaultIsRequired = table.Column<float>(nullable: false),
                    HighIsRequired = table.Column<float>(nullable: false),
                    MaxIsRequired = table.Column<float>(nullable: false),
                    FormulaNameTitle = table.Column<string>(nullable: true),
                    RatingFromTitle = table.Column<bool>(nullable: false),
                    RatingFromInclusiveTitle = table.Column<bool>(nullable: false),
                    RatingToTitle = table.Column<float>(nullable: false),
                    RatingToInclusiveTitle = table.Column<bool>(nullable: false),
                    RatioFromTitle = table.Column<float>(nullable: false),
                    RatioFromInclusiveTitle = table.Column<bool>(nullable: false),
                    RatioToTitle = table.Column<float>(nullable: false),
                    RatioToInclusiveTitle = table.Column<bool>(nullable: false),
                    CustomCriteria0Title = table.Column<string>(nullable: true),
                    CustomCriteria0ValueTitle = table.Column<string>(nullable: true),
                    CustomCriteria0FromValueTitle = table.Column<string>(nullable: true),
                    CustomCriteria0FromInclusiveTitle = table.Column<bool>(nullable: false),
                    CustomCriteria0ToValueTitle = table.Column<string>(nullable: true),
                    CustomCriteria0ToInclusiveTitle = table.Column<bool>(nullable: false),
                    MinTitle = table.Column<float>(nullable: false),
                    LowTitle = table.Column<float>(nullable: false),
                    DefaultTitle = table.Column<float>(nullable: false),
                    HighTitle = table.Column<float>(nullable: false),
                    MaxTitle = table.Column<float>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_matrices", x => x.id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "matrices",
                schema: "compensation");
        }
    }
}
