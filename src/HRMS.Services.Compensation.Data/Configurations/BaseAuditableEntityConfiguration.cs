﻿using HRMS.Common.Types;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace HRMS.Services.Compensation.Data.Configurations
{
    public abstract class BaseAuditableEntityConfiguration<TEntity> : BaseEntityConfiguration<TEntity> where TEntity : BaseAuditableEntity
    {
        public override void Configure(EntityTypeBuilder<TEntity> builder)
        {
            base.Configure(builder);

            builder.Property(x => x.CreatedBy)
                .HasColumnName("created_by")
                .HasColumnType("uuid");

            builder.Property(x => x.CreatedByWithProxy)
                .HasColumnName("created_by_with_proxy")
                .HasColumnType("uuid");

            builder.Property(x => x.ModifiedBy)
                .HasColumnName("modified_by")
                .HasColumnType("uuid");

            builder.Property(x => x.ModifiedByWithProxy)
                .HasColumnName("modified_by_with_proxy")
                .HasColumnType("uuid");
        }
    }
}
