﻿using HRMS.Services.Compensation.Items.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace HRMS.Services.Compensation.Data.Configurations
{
    public class MatrixConfiguration : BaseAuditableEntityConfiguration<Matrix>
    {
        public override void Configure(EntityTypeBuilder<Matrix> builder)
        {
            base.Configure(builder);

            builder.ToTable("matrices");

            builder.Property(x => x.Id)
                   .HasColumnType("uuid")
                   .HasColumnName("id")
                   .IsRequired();

            builder.Property(x => x.Name)
                   .HasColumnType("varchar")
                   .HasColumnName("name")
                   .HasMaxLength(250)
                   .IsRequired();

            builder.Property(x => x.FormulaName)
                   .HasColumnType("varchar")
                   .HasColumnName("formula_name")
                   .HasMaxLength(250)
                   .IsRequired();
        }
    }
}
