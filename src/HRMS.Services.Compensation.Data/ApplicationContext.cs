﻿using HRMS.Services.Compensation.Items.Entities;
using Microsoft.EntityFrameworkCore;

namespace HRMS.Services.Compensation.Data
{
    public class ApplicationContext : DbContext
    {
        private readonly string schema = "compensation";

        public ApplicationContext(DbContextOptions<ApplicationContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema(schema);

            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfigurationsFromAssembly(typeof(DataIdentifierType).Assembly);
        }


        public DbSet<Matrix> Matrices { get; set; }
    }
}
