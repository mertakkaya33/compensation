﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;

namespace HRMS.Services.Compensation.Data
{
    public interface IUnitofWork : IDisposable
    {
        DbSet<T> AsQueryable<T>() where T : class;

        Task<int> SaveChangesAsync();
    }
}
