FROM microsoft/dotnet:2.2-sdk AS build
WORKDIR /app

RUN mkdir -p /HRMS.Services.Compensation/ 
RUN mkdir -p /HRMS.Services.Compensation.Core/
RUN mkdir -p /HRMS.Services.Compensation.Data/ 
RUN mkdir -p /HRMS.Services.Compensation.Items/

COPY ./src/HRMS.Services.Compensation/. ./HRMS.Services.Compensation
COPY ./src/HRMS.Services.Compensation.Core/. ./HRMS.Services.Compensation.Core
COPY ./src/HRMS.Services.Compensation.Data/. ./HRMS.Services.Compensation.Data
COPY ./src/HRMS.Services.Compensation.Items/. ./HRMS.Services.Compensation.Items

COPY ./nuget.config ./src/HRMS.Services.Compensation/

RUN dotnet restore ./HRMS.Services.Compensation/HRMS.Services.Compensation.csproj

RUN dotnet publish ./HRMS.Services.Compensation/ -c Release -o /app/out

FROM microsoft/dotnet:2.2-aspnetcore-runtime AS runtime

WORKDIR /app

COPY --from=build /app/out .

ENV ASPNETCORE_URLS http://*:5000
ENV ASPNETCORE_ENVIRONMENT Docker

EXPOSE 5000

ENTRYPOINT ["dotnet","HRMS.Services.Compensation.dll"]