﻿using System.Collections.Generic;
using System.Linq.Expressions;

namespace HRMS.Services.Compensation.Tests
{
    public class ExpressionInterpreter
    {
        private readonly ExpressionContext expressionContext;

        private readonly Expression expression;

        private List<string> funcList = new List<string>();

        public ExpressionInterpreter(ExpressionContext context)
        {
            expressionContext = context;

            funcList.Add("if");
            funcList.Add("lookup");
            funcList.Add("toNumber");
        }

        public Expression Interpret()
        {
            Expression expression;

            if (expressionContext.ExpressionFormula.Contains("if"))
            {
                expression = InterpretWithFunc();
            }
            else
            {
                expression = InterpretWithoutFunc();
            }

            return expression;
        }

        private Expression InterpretWithoutFunc()
        {
            var split = expressionContext.ExpressionFormula.Split("+");

            return Expression.Empty();
        }

        private Expression InterpretWithFunc()
        {
            return Expression.Empty();
        }


    }
}
