﻿namespace HRMS.Services.Compensation.Tests.Expressions
{
    public abstract class ExpressionsBase
    {
        public abstract void Interpret(ExpressionContext context);
    }
}
